﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SignalR.Client.Hubs;
using Canary.Web.Models.GitHub;
using Newtonsoft.Json;
using Canary.Web.Models;

namespace Canary.Web.Controllers
{
    public class GitHubController : ApiController
    {
        private readonly IHubProxy _canary;

        public GitHubController(IHubProxy canary)
        {
            _canary = canary;
        }

        public bool Post(GitHubRequest request)
        {
            var push = JsonConvert.DeserializeObject<Push>(request.Payload);
            
            var commits = push.commits.Select(c => new SourceCommit()
            {
                Author = c.author.email,
                Branch = push.@ref,
                Changes = c.added.Count + c.modified.Count + c.removed.Count,
                Comment = c.message,
                Hash = c.id,
                Repository = (push.repository != null) ? push.repository.name : null
            });

            foreach (var commit in commits)
            {
                _canary.Invoke("Commit", commit);
            }

            return true;
        }
    }
}
