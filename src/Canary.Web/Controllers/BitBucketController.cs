﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Canary.Web.Models.BitBucket;
using Canary.Web.Models;
using SignalR.Client.Hubs;
using Canary.Web.Infrastructure;
using System.Web;
using System.Net.Http.Formatting;
using Newtonsoft.Json;

namespace Canary.Web.Controllers
{
    public class BitBucketController : ApiController
    {
        private readonly IHubProxy _canary;

        public BitBucketController(IHubProxy canary)
        {
            _canary = canary;
        }

        public bool Post(BitBucketRequest request)
        {
            var push = JsonConvert.DeserializeObject<Push>(request.Payload);
            
            var commits = push.commits.Select(c => new SourceCommit()
            {
                Author = c.author,
                Branch = c.branch,
                Changes = (c.files != null) ? c.files.Length : 0,
                Comment = c.message,
                Hash = c.node,
                Repository = (push.repository != null) ? push.repository.name : null
            });

            foreach (var commit in commits)
            {
                _canary.Invoke("Commit", commit);
            }

            return true;
        }
    }
}
