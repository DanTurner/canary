﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SignalR.Hubs;
using Canary.Web.Models;

namespace Canary.Web.Hubs
{
    [HubName("canary")]
    public class CanaryHub : Hub
    {
        public void Commit(SourceCommit commit)
        {
            Clients.newCommit(commit);
        }
    }
}