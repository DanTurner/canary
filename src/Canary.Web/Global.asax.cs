﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Autofac;
using Canary.Web.Infrastructure;
using Canary.Web.Config;
using System.Configuration;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using System.Web.Http.Tracing;

namespace Canary.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            AppConfig config = AppConfigReader.Read(ConfigurationManager.AppSettings,
                                                    ConfigurationManager.ConnectionStrings);

            var builder = new ContainerBuilder();
            builder.RegisterModule(new WebModule(config));
            IContainer container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            GlobalConfiguration.Configuration.MessageHandlers.Add(new LoggingFilter());

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
    }
}