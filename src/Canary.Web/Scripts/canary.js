﻿$(function () {
    function generateGuid() {
        var result, i, j;
        result = '';
        for (j = 0; j < 32; j++) {
            if (j == 8 || j == 12 || j == 16 || j == 20)
                result = result + '-';
            i = Math.floor(Math.random() * 16).toString(16).toUpperCase();
            result = result + i;
        }
        return result
    }

    // Proxy created on the fly
    var canary = $.connection.canary;

    // Declare a function on the canary hub so the server can invoke it
    canary.addMessage = function (message) {
        $('#messages').append('<li>' + message + '</li>');
    };

    canary.newCommit = function (commit) {
        var message = commit.Repository + " - " + commit.Hash + " - " + commit.Comment;
        console.log(commit);
        $('#messages').append('<li>' + message + '</li>');
    }

    $("#broadcast").click(function () {
        var commit = {
            repository: 'Testing',
            hash: generateGuid(),
            comment: $('#msg').val()
        };

        // Call the canary method on the server
        canary.commit(commit);
    });

    // Start the connection
    $.connection.hub.start();
});