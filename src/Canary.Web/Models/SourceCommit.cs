﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Canary.Web.Models
{
    public class SourceCommit
    {
        public string Repository { get; set; }
        public string Hash { get; set; }
        public string Author { get; set; }
        public string Branch { get; set; }
        public string Comment { get; set; }
        public int Changes { get; set; }
    }
}