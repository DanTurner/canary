﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Canary.Web.Models.GitHub
{
    public class Committer
    {
        public string name { get; set; }
        public string email { get; set; }
    }

    public class Repository
    {
        public string name { get; set; }
        public int size { get; set; }
        public bool has_wiki { get; set; }
        public string created_at { get; set; }
        public bool @private { get; set; }
        public int watchers { get; set; }
        public string url { get; set; }
        public bool fork { get; set; }
        public string pushed_at { get; set; }
        public int open_issues { get; set; }
        public bool has_downloads { get; set; }
        public bool has_issues { get; set; }
        public int forks { get; set; }
        public string description { get; set; }
        public Committer owner { get; set; }
    }

    public class User
    {
        public string name { get; set; }
        public string username { get; set; }
        public string email { get; set; }
    }

    public class Commit
    {
        public List<string> added { get; set; }
        public List<string> modified { get; set; }
        public List<string> removed { get; set; }
        public string timestamp { get; set; }
        public User author { get; set; }
        public string url { get; set; }
        public string id { get; set; }
        public bool distinct { get; set; }
        public string message { get; set; }
        public User committer { get; set; }
    }

    public class Push
    {
        public Committer pusher { get; set; }
        public Repository repository { get; set; }
        public bool forced { get; set; }
        public Commit head_commit { get; set; }
        public string after { get; set; }
        public bool deleted { get; set; }
        public List<Commit> commits { get; set; }
        public string @ref { get; set; }
        public string before { get; set; }
        public string compare { get; set; }
        public bool created { get; set; }
    }
}