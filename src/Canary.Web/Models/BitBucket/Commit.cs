﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Canary.Web.Models.BitBucket
{
    public class Commit
    {
        public string author { get; set; }
        public string branch { get; set; }
        public string message { get; set; }
        public string node { get; set; }
        public string raw_author { get; set; }
        public int? revision { get; set; }
        public int size { get; set; }
        public DateTime timestamp { get; set; }
        public DateTime utctimestamp { get; set; }
        public CommitFile[] files { get; set; }
        public string[] parents { get; set; }
    }
}