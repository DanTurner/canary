﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Canary.Web.Models.BitBucket
{
    public class Push
    {
        public string canon_url { get; set; }
        public string user { get; set; }
        public Commit[] commits { get; set; }
        public Repository repository { get; set; }
    }
}