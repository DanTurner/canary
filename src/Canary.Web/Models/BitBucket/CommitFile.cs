﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Canary.Web.Models.BitBucket
{
    public class CommitFile
    {
        public string file { get; set; }
        public string type { get; set; }
    }
}