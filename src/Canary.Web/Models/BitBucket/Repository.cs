﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Canary.Web.Models.BitBucket
{
    public class Repository
    {
        public string absolute_url { get; set; }
        public bool fork { get; set; }
        public bool is_private { get; set; }
        public string name { get; set; }
        public string owner { get; set; }
        public string scm { get; set; }
        public string slug { get; set; }
        public string website { get; set; }
    }
}