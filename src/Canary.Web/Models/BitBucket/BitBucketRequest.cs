﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Canary.Web.Models.BitBucket
{
    public class BitBucketRequest
    {
        public string Payload { get; set; }
    }
}