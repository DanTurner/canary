﻿using System.Collections.Specialized;
using System.Configuration;

namespace Canary.Web.Config
{
    public class AppConfigReader
    {
        public static AppConfig Read(NameValueCollection appSettings,
                                     ConnectionStringSettingsCollection connectionStrings)
        {
            string hubUrl = appSettings["HubUrl"];

            return new AppConfig()
            {
                HubUrl = hubUrl
            };
        }
    }
}