﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using Canary.Web.Config;
using SignalR.Client.Hubs;

namespace Canary.Web.Infrastructure
{
    public class WebModule : Module
    {
        private readonly AppConfig _config;

        public WebModule(AppConfig config)
        {
            _config = config;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(cfg =>
            {
                var connection = new HubConnection(_config.HubUrl);
                var canary = connection.CreateProxy("canary");
                connection.Start().Wait();
                return canary;
            }).As<IHubProxy>();

            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterApiControllers(typeof(MvcApplication).Assembly);
        }
    }
}