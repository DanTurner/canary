﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Canary.Web.Infrastructure
{
    public class ApiLoggingInfo
    {
        public ApiLoggingInfo()
        {
            Headers = new Dictionary<string, IList<string>>();
        }

        public string HttpMethod { get; set; }

        public string UriAccessed { get; set; }

        public string IPAddress { get; set; }

        public string BodyContent { get; set; }

        public IDictionary<string, IList<string>> Headers { get; set; }

        public void AddHeader(string key, string value)
        {
            var keyLower = key.ToLower();
            if (!Headers.ContainsKey(keyLower))
            {
                Headers.Add(keyLower, new List<string>());
            }
            Headers[keyLower].Add(value);
        }
    }
}
