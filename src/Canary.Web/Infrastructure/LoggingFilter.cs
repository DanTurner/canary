﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;
using System.Threading.Tasks;
using System.Net.Http;
using System.Web.Http.Filters;
using System.Threading;
using System.Web.Script.Serialization;

namespace Canary.Web.Infrastructure
{
    internal class LoggingFilter : DelegatingHandler
    {
        protected override System.Threading.Tasks.Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, System.Threading.CancellationToken cancellationToken)
        {
            // Extract the request logging information
            var requestLoggingInfo = ExtractLoggingInfoFromRequest(request);

            // Execute the request, this does not block
            var response = base.SendAsync(request, cancellationToken);

            // Log the incoming data to the database
            WriteLoggingInfo(requestLoggingInfo);

            return response;
        }

        private void WriteLoggingInfo(ApiLoggingInfo requestLoggingInfo)
        {
            JavaScriptSerializer serialise = new JavaScriptSerializer();
            var result = serialise.Serialize(requestLoggingInfo);
            new LogEvent(result).Raise();
        }

        private ApiLoggingInfo ExtractLoggingInfoFromRequest(HttpRequestMessage request)
        {
            var info = new ApiLoggingInfo();
            info.HttpMethod = request.Method.Method;
            info.UriAccessed = request.RequestUri.AbsoluteUri;
            info.IPAddress = HttpContext.Current != null ? HttpContext.Current.Request.UserHostAddress : "0.0.0.0";

            ExtractMessageHeadersIntoLoggingInfo(info, request.Headers.ToList());
            if (request.Content != null)
            {
                request.Content.ReadAsByteArrayAsync()
                    .ContinueWith((task) =>
                    {
                        info.BodyContent = System.Text.UTF8Encoding.UTF8.GetString(task.Result);
                        return info;

                    });
            }
            return info;
        }

        private void ExtractMessageHeadersIntoLoggingInfo(ApiLoggingInfo info, List<KeyValuePair<string, IEnumerable<string>>> list)
        {
            foreach (var kvp in list)
            {
                foreach (var item in kvp.Value)
                {
                    info.AddHeader(kvp.Key, item);
                }
            }
        }
    }
}